// App.js
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './screens/HomeScreen';
import MainScreen from './screens/MainScreen';
import Login from './screens/Login';
import Bottomtab from './screens/Bottomtab';
import SignUp from './screens/SignUp';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='HomeScreen' component={HomeScreen} options={{ headerShown: false }} />
        <Stack.Screen name='MainScreen' component={MainScreen} options={{ headerShown: false }}/>
        <Stack.Screen name='Login' component={Login} 
            options={{
              title:'',
            }}
            />
            <Stack.Screen name='SignUp' component={SignUp}

            options={{
              title:'',
            }}
            />
            <Stack.Screen name='Bottomtab' component={Bottomtab}options={{ headerShown: false }} /> 
            
      </Stack.Navigator>

    </NavigationContainer>
  );
}

export default App;






