// //Add.js
// import React, { useState } from 'react';
// import { View, Text, Pressable, StyleSheet, TextInput } from 'react-native';
// import { useNavigation } from '@react-navigation/native';
// import { useDispatch } from 'react-redux';
// import { addNote } from '../../redux/actions/authActions';
// import SQLite from 'react-native-sqlite-storage';

// const db = SQLite.openDatabase(
//   { name: 'ninad.db', location: 'default' },
//   (db) => {
//     console.log("DataBasE ConNecteD");
//     db.executeSql(
//       'CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, note TEXT, color TEXT)',
//       [],
//       () => {
//         console.log('Table "notes" created successfully');
//       },
//       (error) => {
//         console.error('Error creating table "notes":', error);
//       }
//     );
//   },
//   (error) => {
//     console.error('Error opening database:', error);
//   }
// );

// const Add = () => {
//   const navigation = useNavigation();
//   const dispatch = useDispatch();
//   const [note, setNote] = useState('');
//   const [selectedColor, setSelectedColor] = useState('');

//   const addGoal = () => {
//     if (note.trim() === '') {
      
//       console.log('Note cannot be empty');
//       return;
//     }
//     const noteObject = { note, color: selectedColor };

//     dispatch(addNote(noteObject));

//     db.transaction(
//       (tx) => {
//         tx.executeSql(
//           'INSERT INTO notes (note, color) VALUES (?, ?)',
//           [note, selectedColor],
//           (tx, results) => {
//             console.log('Results', results);
//           },
//           (error) => {
//             console.error(error);
//           }
//         );
//       },
//       (error) => {
//         console.error(error);
//       }
//     );

//     setNote('');
//     setSelectedColor('');
//     navigation.navigate('Quick Notes',{note : note});
//   };

//   return (
//     <View elevation={10} style={styles.Con}>
//       <Text style={styles.Text}>Description</Text>
//       <TextInput
//         placeholder="Enter your Notes here"
//         placeholderTextColor="gray"
//         textAlignVertical="top"
//         multiline={true}
//         numberOfLines={60}
//         style={styles.TextInput}
//         value={note}
//         onChangeText={(text) => setNote(text)}
//       />

//       <Text style={styles.Text}>Choose Color</Text>
//       <View style={styles.Con2}>
//         <Pressable
//           style={[styles.View, { backgroundColor: '#6074f9' }]}
//           onPress={() => setSelectedColor('#6074f9')}
//         />
//         <Pressable
//           style={[styles.View, { backgroundColor: '#e42b6a' }]}
//           onPress={() => setSelectedColor('#e42b6a')}
//         />
//         <Pressable
//           style={[styles.View, { backgroundColor: '#5abb56' }]}
//           onPress={() => setSelectedColor('#5abb56')}
//         />
//         <Pressable
//           style={[styles.View, { backgroundColor: '#3d3a62' }]}
//           onPress={() => setSelectedColor('#3d3a62')}
//         />
//         <Pressable
//           style={[styles.View, { backgroundColor: '#f4ca8f' }]}
//           onPress={() => setSelectedColor('#f4ca8f')}
//         />
//       </View>
//       <Pressable style={styles.Btn} onPress={addGoal}>
//         <Text style={styles.Text2}>Done</Text>
//       </Pressable>
//     </View>
//   );
// };




// const styles = StyleSheet.create({
//   Con: {
//     zIndex: 1,
//     marginTop: -60,
//     marginHorizontal: '7%',
//     padding: '5%',
//     backgroundColor: 'white'
//   },
//   Text: {
//     color: 'black',
//     marginVertical: 18,
//     fontSize: 20
//   },
//   TextInput: {
//     height: 160,
//     fontSize: 20
//   },
//   Con2: {
//     width: '100%',
//     flexDirection: 'row',
//     justifyContent: 'space-evenly',
//   },
//   View1: {
//     backgroundColor: '#6074f9',
//     borderRadius: 5,
//     height: 50,
//     width: 50
//   },
//   View2: {
//     backgroundColor: '#e42b6a',
//     borderRadius: 5,
//     height: 50,
//     width: 50
//   },
//   View3: {
//     backgroundColor: '#5abb56',
//     borderRadius: 5,
//     height: 50,
//     width: 50
//   },
//   View4: {
//     backgroundColor: '#3d3a62',
//     borderRadius: 5,
//     height: 50,
//     width: 50
//   },
//   View5: {
//     backgroundColor: '#f4ca8f',
//     borderRadius: 5,
//     height: 50,
//     width: 50
//   },
//   Btn: {
//     width: '90%',
//     height: 50,
//     justifyContent: 'center',
//     alignSelf: 'center',
//     backgroundColor: '#fb6464',
//     marginVertical: 40,
//     borderRadius: 5
//   },
//   Text2: {
//     color: 'white',
//     fontSize: 20,
//     alignSelf: 'center'
//   },
//   View: {
//     borderRadius: 5,
//     height: 50,
//     width: 50,
//   },

// })


// export default Add;





import React, { useState ,useEffect} from 'react';
import { View, Text, TextInput, TouchableOpacity, Pressable, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { addNote } from '../../redux/actions/authActions';
import SQLite from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';

const db = SQLite.openDatabase({ name: 'new.db', location: 'default' });

const Add = () => {
     


  const navigation = useNavigation();
  const [note, setNote] = useState('');
  const [selectedColor, setSelectedColor] = useState('#6074f9'); 
  const dispatch = useDispatch();
  const [username, setUsername] = useState('');

  useEffect(() => {
    // const fetchUsername = async () => {
    //   try {
    //     const storedUsername = await AsyncStorage.getItem('Name');
    //     if (storedUsername) {
    //       console.log('Fetched username from AsyncStorage:', storedUsername);
    //       setUsername(storedUsername);
    //     }
    //   } catch (error) {
    //     console.error('Error retrieving username from AsyncStorage', error);
    //   }
    // };

    // fetchUsername();

    db.transaction((tx) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, note TEXT, color TEXT)',
        [],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            console.log('Table "notes" created or already exists');
          }
        },
        
      );
    });
  }, []);

  const addGoal = () => {
    if (note.trim() === '') {
      alert('Note cannot be empty');
      return;
    }
  
    dispatch(addNote({ username, note, color: selectedColor }));
  
    db.transaction((tx) => {
      tx.executeSql(
        'INSERT INTO notes (username, note, color) VALUES (?, ?, ?)',
        [username, note, selectedColor],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            console.log('Note added successfully to SQLite database', { note, selectedColor });
          } else {
            // console.error('Failed to add note to SQLite database');
          }
        },
        (error) => {
          console.error('Error inserting note into SQLite database', error);
        }
      );
    });
    console.log('INSERT INTO statement executed.');
  
    navigation.navigate('Quick Notes');
  };

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.textdescription}>Description</Text>
        <View style={styles.textInputContainer}>
          <TextInput
            style={styles.textInput}
            placeholder='Enter a note'
            multiline={true}
            value={note}
            onChangeText={(text) => setNote(text)}
          />
        </View>
      </View>
      <View style={styles.container2}>
        <Text style={styles.textColor}>Choose Color</Text>
        <View style={styles.colorContainer}>
          <Pressable onPress={() => setSelectedColor('#6074f9')}>
            <Text style={[styles.colorBox1, { backgroundColor: '#6074f9' }]}></Text>
          </Pressable>
          <Pressable onPress={() => setSelectedColor('#e42b6a')}>
            <Text style={[styles.colorBox2, { backgroundColor: '#e42b6a' }]}></Text>
          </Pressable>
          <Pressable onPress={() => setSelectedColor('#5abb56')}>
            <Text style={[styles.colorBox3, { backgroundColor: '#5abb56' }]}></Text>
          </Pressable>
          <Pressable onPress={() => setSelectedColor('#3d3a62')}>
            <Text style={[styles.colorBox4, { backgroundColor: '#3d3a62' }]}></Text>
          </Pressable>
          <Pressable onPress={() => setSelectedColor('#f4ca8f')}>
            <Text style={[styles.colorBox5, { backgroundColor: '#f4ca8f' }]}></Text>
          </Pressable>
        </View>
      </View>
      <View>
        <TouchableOpacity style={styles.done} onPress={addGoal}>
          <Text style={styles.textdone}>Done</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Add;

const styles = StyleSheet.create({
  container: {
    width: '90%',
    height: '90%',
    marginHorizontal: '5%',
    flexDirection: 'column',
    padding: 20,
    backgroundColor: 'white',
    borderRadius: 8,
    elevation: 5,
    zIndex: 1,
    marginTop: -60,
  },
  textInputContainer: {
    minHeight: 200,
    marginVertical: 10,
    borderWidth: 0.5,
    borderRadius: 8,
    borderColor: 'white'
  },
  textInput: {
    padding: 10,
    fontSize: 20,
  },
  done: {
    backgroundColor: '#F96060',
    padding: 15,
    alignItems: 'center',
    margin: 25,
    borderRadius: 5,
  },
  textdone: {
    fontSize: 20,
    color: 'white',
  },
  textdescription: {
    fontSize: 20,
    marginBottom: 10,
  },
  textColor: {
    fontSize: 20,
  },
  container2: {
    marginBottom: '5%',
    marginTop: '3%',
    marginHorizontal: 5,
  },
  colorBox1: {
    height: 50,
    width: 50,
    borderRadius: 8,
    backgroundColor: '#6074f9'
  },
  colorBox2: {
    height: 50,
    width: 50,
    borderRadius: 8,
    backgroundColor: '#e42b6a'
  },
  colorBox3: {
    height: 50,
    width: 50,
    borderRadius: 8,
    backgroundColor: '#5abb56'
  },
  colorBox4: {
    height: 50,
    width: 50,
    borderRadius: 8,
    backgroundColor: '#3d3a62'
  },
  colorBox5: {
    height: 50,
    width: 50,
    borderRadius: 8,
    backgroundColor: '#f4ca8f'
  },
  colorContainer: {
    flexDirection: 'row',
    marginTop: '5%',
    justifyContent: 'space-evenly'
  }
});












