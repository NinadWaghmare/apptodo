import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MyTask from './MyTask';
import Menu from './Menu';
import Profile from './Profile';
import Add from './Add';
import React from 'react';
import QuickNotes from '../screens/QuickNotes'

const Tab = createBottomTabNavigator();

const CustomTabBarButton = ({ children, onPress }) => (
  <TouchableOpacity style={styles.tabBarButton} onPress={onPress}>
    <View style={styles.redCircle}>
      <View style={styles.Plus}>{children}</View>
    </View>
  </TouchableOpacity>
);

const Bottomtab = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarStyle: { backgroundColor: '#242442', height: 80 },
        tabBarActiveTintColor: 'white',
        tabBarInactiveTintColor: 'gray',
        tabBarLabelStyle: {
          marginBottom: 10,
          fontSize: 15,
        },
        tabBarIcon: ({ color, size }) => {
          let iconName;

          if (route.name === 'My Task') {
            iconName = 'check-circle-outline';
          } else if (route.name === 'Menu') {
            iconName = 'microsoft';
          } else if (route.name === 'Add') {
            iconName = 'plus';
          } else if (route.name === 'Quick Notes') {
            iconName = 'notebook-check';
          } else if (route.name === 'Profile') {
            iconName = 'account';
          }

          return <MaterialCommunityIcons name={iconName} color={color} size={35} />;
        },
      })}
    >
      <Tab.Screen name="My Task" component={MyTask} />
      <Tab.Screen name="Menu" component={Menu} />
      <Tab.Screen name='Add' component={Add}
      options={{ 
        headerTitle:'Add Notes',
        headerStyle: { backgroundColor: '#fb6464', height: 180},
        headerTitleAlign: 'center',
        headerTintColor:'white',
        tabBarLabel :'',
        tabBarIcon:({focused,color,size})=>(
          < MaterialCommunityIcons name="plus" color={'white'} size={32} />
        ),
        tabBarButton:(props)=>(
          <CustomTabBarButton {...props}/>
        )
        }}/>
      <Tab.Screen name="Quick Notes" component={QuickNotes} options={{headerTitleAlign: 'center'}} />
      <Tab.Screen name="Profile" component={Profile} />
     
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  tabBarButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  redCircle: {
    width: 60,
    height: 60,
    borderRadius: 35,
    backgroundColor: '#e17070',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
    position: 'relative',
  },
  Plus: {
    bottom: -12,
    alignItems: 'center'
  },
});

export default Bottomtab;
