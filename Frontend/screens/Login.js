
// // Login.js
// import React, { useState } from 'react';
// import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
// import { useNavigation } from '@react-navigation/native';
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import { useSelector,useDispatch } from 'react-redux';
// import { clearUser, setUser } from '../../redux/actions/authActions';

// const Login = () => {
//   const navigation = useNavigation();
//   const dispatch = useDispatch();
//   const user = useSelector((state) => state.auth.user);

//   const [username, setUsername] = useState('');
//   const [password, setPassword] = useState('');

//   const handleLogin = async () => {
//     // Retrieve existing users from AsyncStorage
//     const existingUsers = JSON.parse(await AsyncStorage.getItem('users')) || [];

//     // Find the user with the entered username
//     const foundUser = existingUsers.find(user => user.username === username);

//     if (foundUser && foundUser.password === password) {
//       // Dispatch user to Redux store
//       dispatch(setUser({ username }));

//       navigation.navigate('Bottomtab');
//     } else {
//       dispatch(clearUser());
//       alert('Invalid username or password. Please check your credentials.');
//     }
//   };

//   return (
//     <View style={styles.container}>
//       <View style={styles.subContainer}>
//         <Text style={styles.textHead}>Welcome back</Text>
//         <Text style={styles.text}>Sign in to Continue</Text>

//         <View style={styles.view2}>
//           <Text style={styles.inputText}>Email</Text>
//           <TextInput
//             style={styles.textInput}
//             placeholder='Enter your email'
//             value={username}
//             onChangeText={(text) => setUsername(text)}
//           />

//           <Text style={[styles.inputText, { marginTop: 30 }]}>Password</Text>
//           <TextInput
//             style={styles.textInput}
//             placeholder='Enter your password'
//             secureTextEntry
//             value={password}
//             onChangeText={(text) => setPassword(text)}
//           />
//           <TouchableOpacity style={styles.signUp} onPress={() => navigation.navigate('SignUp')}>
//             <Text style={styles.inputText}>Sign Up</Text>
//           </TouchableOpacity>
//         </View>

//         <TouchableOpacity style={styles.Login} onPress={handleLogin}>
//           <Text style={styles.text2}>Log In</Text>
//         </TouchableOpacity>
//       </View>
//     </View>
//   );
// };

// export default Login;




// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     marginTop: 0,
//   },
//   subContainer: {
//     height: '80%',
//     marginTop: '15%',
//     justifyContent:'center',
//     alignItems:'center'
//   },
//   textHead: {
//     margin: 20,
//     fontSize: 30,
//     color: 'black',
//     opacity: 1,
//   },
//   text: {
//     fontSize: 20,
//     color: 'black',
//     marginLeft: 20,
//     opacity: 0.5,
//   },
//   view2: {
//     marginTop: 30,
//     padding: 30,
//   },
//   inputText: {
//     fontSize: 24,
//     color: 'black',
//     opacity: 1,
//     padding: 5,
//   },
//   textInput: {
//     borderBottomWidth: 1,
//     opacity: 0.5,
//     fontSize: 19,
//   },
//   signUp: {
//     marginLeft: '70%',
//     padding: 5,
//   },
//   Login: {
//     backgroundColor: '#FF5050',
//     padding: 15,
//     paddingHorizontal:100,
//     margin: 25,
//     borderRadius: 8,
//   },
//   text2: {
//     fontSize: 24,
//     color: '#fff',
//     textAlign: 'center',
//   },
// });





import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import { clearUser, setUser } from '../../redux/actions/authActions';

const Login = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async () => {
    const storedUsersString = await AsyncStorage.getItem('users');
    const storedUsers = storedUsersString ? JSON.parse(storedUsersString) : [];

    // Find the user by username
    const foundUser = storedUsers.find(user => user.username === username);

    if (foundUser && foundUser.password === password) {
      dispatch(setUser({ username }));
      await AsyncStorage.setItem("isLogin", "true")
      navigation.navigate('Bottomtab');
    } else {
      // dispatch(clearUser());
      alert('Invalid username or password. Please sign up first.');
    }
  };

  return (
        <View style={styles.container}>
          <View style={styles.subContainer}>
            <Text style={styles.textHead}>Welcome back</Text>
            <Text style={styles.text}>Sign in to Continue</Text>
    
            <View style={styles.view2}>
              <Text style={styles.inputText}>Username</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Enter your email'
                value={username}
                onChangeText={(text) => setUsername(text)}
              />
    
              <Text style={[styles.inputText, { marginTop: 30 }]}>Password</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Enter your password'
                secureTextEntry
                value={password}
                onChangeText={(text) => setPassword(text)}
              />
              <TouchableOpacity style={styles.signUp} onPress={() => navigation.navigate('SignUp')}>
                <Text style={styles.inputText}>Sign Up</Text>
              </TouchableOpacity>
            </View>
    
            <TouchableOpacity style={styles.Login} onPress={handleLogin}>
              <Text style={styles.text2}>Log In</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    };
    
    export default Login;
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: '#fff',
        marginTop: 0,
      },
      subContainer: {
        height: '80%',
        marginTop: '15%',
      },
      textHead: {
        margin: 20,
        fontSize: 30,
        color: 'black',
        opacity: 1,
      },
      text: {
        fontSize: 20,
        color: 'black',
        marginLeft: 20,
        opacity: 0.5,
      },
      view2: {
        marginTop: 30,
        padding: 30,
      },
      inputText: {
        fontSize: 24,
        color: 'black',
        opacity: 1,
        padding: 5,
      },
      textInput: {
        borderBottomWidth: 1,
        opacity: 0.5,
        fontSize: 19,
      },
      signUp: {
        marginLeft: '70%',
        padding: 5,
      },
      Login: {
        backgroundColor: '#FF5050',
        padding: 15,
        margin: 25,
        borderRadius: 5,
      },
      text2: {
        fontSize: 24,
        color: '#fff',
        textAlign: 'center',
      },
    });























































