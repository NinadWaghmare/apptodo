//MainScreen.js
import { View, Text, StyleSheet, Image, TouchableOpacity, Button } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native';
const MainScreen = () => {
  const navigation = useNavigation()
  return (
    <View style={styles.container}>
      <View style={styles.image4View}>
      <Image style={styles.image4} source={require('../Images/Page21.png')}/>
      <Text style={styles.btnText}>Welcome to Tech Titans</Text>
      <Text style={{fontSize:18,padding:10}}>Whats going to happen tomorrow?</Text>
      </View>
      <View style={styles.view}>
      <Image style={styles.image} source={require('../Images/Group3.png')}/>
      <TouchableOpacity style={styles.btnStyle} 
      onPress={()=>{
        navigation.navigate('SignUp')

      }} >
          <Text style={styles.btnText}>Sign Up</Text>
        </TouchableOpacity>
        <TouchableOpacity
        onPress={()=>{
          navigation.navigate('Login')
    
          }}
        >
          <Text style={styles.btnLogin}>Log In</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default MainScreen
const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#fff'
  },
  view:{
    height:'53%',
  },
  image:{
    width:'100%',
    height:'100%',
    marginTop:'32%',
    position:'absolute',
    
  },
  btnStyle:{
    backgroundColor:'white',
    padding:12,
    borderRadius:5,
    width:'80%',
    marginTop:'65%',
    margin:'10%'
  },
  btnText:{
    fontSize:22,
    color:'black',
    textAlign:'center',
   
  },
  btnLogin:{
    color:'white',
    fontSize:22,
    textAlign:'center',
  },
  image4View:{
    justifyContent:'center',
    alignItems:'center'
  },
  image4:{
    width:300,
    height:300,
    marginTop:20
  }


})

