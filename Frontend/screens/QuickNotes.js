
// //QuickNotes.js
// import React, { useEffect } from 'react';
// import { View, Text, StyleSheet, FlatList } from 'react-native';
// import { useSelector, useDispatch } from 'react-redux';
// import { setNotes } from '../../redux/actions/authActions'; // Adjust the import path as needed
// import SQLite from 'react-native-sqlite-storage';

// const db = SQLite.openDatabase(
//   { name: 'ninad.db', location: 'default' },
//   (db) => {
//     console.log("Database Connected");
//   },
//   (error) => {
//     console.error('Error opening database:', error);
//   }
// );

// const QuickNotes = () => {
//   const notesWithColor = useSelector((state) => state.notes.notesWithColor);
//   const dispatch = useDispatch();

//   useEffect(() => {
//     // Fetch notes from SQLite and dispatch to Redux store
//     db.transaction(
//       (tx) => {
//         tx.executeSql(
//           'SELECT * FROM notes',
//           [],
//           (_, { rows }) => {
//             const fetchedNotes = rows._array || [];
//             dispatch(setNotes(fetchedNotes));
//           },
//           (error) => {
//             console.error(error);
//           }
//         );
//       },
//       (error) => {
//         console.error(error);
//       }
//     );
//   }, [dispatch]);

//   return (
//     <View style={styles.container}>
//       <View>
//         {notesWithColor.map((item, index) => (
//           <View
//             key={index}
//             style={[styles.subContainer, { borderTopColor: item.color || 'white' }]}
//           >
//             <Text style={{ color: 'black' }}>{item.note}</Text>
//           </View>
//         ))}
//       </View>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   subContainer: {
//     zIndex: 1,
//     marginTop: 20,
//     borderRadius: 7,
//     marginHorizontal: '7%',
//     padding: '5%',
//     backgroundColor: 'white',
//     borderTopWidth: 5,
//   },
// });

// export default QuickNotes;





import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector } from 'react-redux';

const db = SQLite.openDatabase({ name: 'new.db', location: 'default' });


const QuickNotes = () => {
  const [notes, setNotes] = useState([]);
  const [username, setUsername] = useState('');
  // const notesWithColor = useSelector((state)=>state.notes.notesWithColor);
  const reduxNotes = useSelector((state) => state.notes.notes);

  useEffect(() => {
    const fetchUsername = async () => {
      try {
        const storedUsername = await AsyncStorage.getItem('Name');
        if (storedUsername) {
          console.log('Fetched username from AsyncStorage:', storedUsername);
          setUsername(storedUsername);
        }
      } catch (error) {
        console.error('Error retrieving username from AsyncStorage', error);
      }
    };

    fetchUsername();
  }, []);

  useEffect(() => {
    console.log('Fetching notes for username:', username);
    fetchNotesFromDatabase();
  }, [username, reduxNotes]);

  const fetchNotesFromDatabase = () => {
    db.transaction((tx) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, note TEXT, color TEXT)',
        [],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            console.log('Table "notes" created or already exists');
          }
        },
        (error) => {
          console.error('Error creating table "notes"', error);
        }
      );

      tx.executeSql(
        'SELECT * FROM notes WHERE username = ?',
        [username],
        (tx, results) => {
          const len = results.rows.length;
          const notesArray = [];
          for (let i = 0; i < len; i++) {
            const row = results.rows.item(i);
            notesArray.push({ note: row.note, color: row.color });
          }
          setNotes(notesArray);
        },
        (error) => {
          console.error('Error retrieving notes from SQLite database', error);
        }
      );
    });
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={notes.concat(reduxNotes)} 
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (

          <View style={styles.subContainer}>
            <View style={[styles.Con2,{ borderTopColor: item.color ,borderTopWidth:4, width:'70%' || 'white' }]}></View>
            <Text style={styles.note}>{item.note}</Text>

          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex:1,
  },
  subContainer: {
    zIndex:1,
    backgroundColor: 'white',
    marginHorizontal:'6%',
    margin: 10,
    borderRadius: 8,
    elevation: 5,
    width:'90%',
    paddingHorizontal:20,
    marginVertical: 10

  },
  note: {
    fontSize: 20,
     padding:6,
    fontWeight: 'bold',
    color: '#242020',
    opacity: 0.9,
    marginBottom: 10
  },
  Con2: {
    height:4,
  marginBottom: 10,
  }
});

export default QuickNotes;

