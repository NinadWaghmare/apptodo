

// // SignUp.js
// import { View, Text, StyleSheet, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
// import React, { useState } from 'react';
// import { useNavigation } from '@react-navigation/native';
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import { useDispatch } from 'react-redux';
// import { setUser } from '../../redux/actions/authActions';

// const SignUp = () => {
//   const navigation = useNavigation();
//   const dispatch = useDispatch();

//   const [username, setUsername] = useState('');
//   const [password, setPassword] = useState('');
//   const [confirmPassword, setConfirmPassword] = useState('');

//   const [usernameError, setUsernameError] = useState('');
//   const [passwordError, setPasswordError] = useState('');
//   const [confirmPasswordError, setConfirmPasswordError] = useState('');

//   const handleSignUp = async () => {
//     setUsernameError('');
//     setPasswordError('');
//     setConfirmPasswordError('');

//     let isValid = true;

//     const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//     if (!username.trim() || !emailRegex.test(username)) {
//       setUsernameError('Please enter a valid email');
//       isValid = false;
//     }

//     const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
//     if (!password.trim() || !passwordRegex.test(password)) {
//       setPasswordError(
//         'Password should contain uppercase and lowercase letters, numbers, special characters, and have a minimum length of 8 characters'
//       );
//       isValid = false;
//     }

//     if (password !== confirmPassword) {
//       setConfirmPasswordError('Passwords do not match');
//       isValid = false;
//     }

//     if (isValid) {
//       // Retrieve existing users from AsyncStorage
//       const existingUsers = JSON.parse(await AsyncStorage.getItem('users')) || [];

//       // Check if the username already exists
//       const userExists = existingUsers.some(user => user.username === username);
//       if (userExists) {
//         setUsernameError('Username already exists. Choose a different one.');
//         return;
//       }

//       // Save the new user
//       const newUser = { username, password, confirmPassword };
//       const updatedUsers = [...existingUsers, newUser];
//       await AsyncStorage.setItem('users', JSON.stringify(updatedUsers));

//       dispatch(setUser({ username }));

//       navigation.navigate('Login');
//     }
//   };

//   return (
//     <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.container}>
//       <View style={styles.subContainer}>
//         <Text style={styles.textHead}>New User ?</Text>
//         <Text style={styles.text}>Sign Up to Continue</Text>

//         <View style={styles.view2}>
//           <Text style={styles.inputText}>Username</Text>
//           <TextInput
//             style={styles.textInput}
//             placeholder='Enter your email'
//             value={username}
//             onChangeText={(text) => setUsername(text)}
//           />
//           <Text style={styles.errorText}>{usernameError}</Text>

//           <Text style={[styles.inputText, { marginTop: 30 }]}>Password</Text>
//           <TextInput
//             style={styles.textInput}
//             placeholder='Enter your password'
//             secureTextEntry
//             value={password}
//             onChangeText={(text) => setPassword(text)}
//           />
//           <Text style={styles.errorText}>{passwordError}</Text>

//           <Text style={[styles.inputText, { marginTop: 30 }]}>Confirm Password</Text>
//           <TextInput
//             style={styles.textInput}
//             placeholder='Enter your password'
//             secureTextEntry
//             value={confirmPassword}
//             onChangeText={(text) => setConfirmPassword(text)}
//           />
//           <Text style={styles.errorText}>{confirmPasswordError}</Text>
//         </View>

//         <TouchableOpacity style={styles.Login} onPress={handleSignUp}>
//           <Text style={styles.text2}>Sign Up</Text>
//         </TouchableOpacity>
//       </View>
//     </KeyboardAvoidingView>
//   );
// };



// export default SignUp;

// const styles = StyleSheet.create({
//   conatiner:{
//    flex:1,
//    backgroundColor:'#fff',
//    marginTop:0
//   },
//   subContainer:{
//     height:'80%',
//     marginTop:25,
//   },
//   textHead:{
//     margin:20,
//     fontSize:30,
//     color:'black',
//     opacity:1,
//   },
//   text:{
//     fontSize:20,
//     color:'black',
//     marginLeft:20,
//     opacity:0.5,
//   },
//   view2:{
//     marginTop:30,
//     padding:30,
//   },
//   inputText:{
//     fontSize:24,
//     color:'black',
//     opacity:1,
//   },
//   textInput:{
//     borderBottomWidth:1,
//     opacity:0.5,
//     fontSize:19,
//   },
//   signUp:{
//     marginLeft:'70%',
//     padding:5,

//   },
//   Login:{
//     backgroundColor:'#FF5050',
//     padding:15,
//     margin:25,
//     borderRadius:5,
//   },
//   text2:{
//     fontSize:24,
//     color:'#fff',
//     textAlign:'center'
//   },
//   errorText: {
//     color: 'red',
//     fontSize: 16,
//     marginTop: 5,
//   },
// })









import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, Pressable, KeyboardAvoidingView, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import { setUser } from '../../redux/actions/authActions';

const SignUp = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const [passwordError, setPasswordError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');

  const handleSignUp = async () => {
    // Reset previous errors
    setPasswordError('');
    setConfirmPasswordError('');

    // Add your validation logic here
    let isValid = true;

    // Password Validation using Regex
    const passwordRegex =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (!password.trim() || !passwordRegex.test(password)) {
      setPasswordError(
        'Password should contain uppercase and lowercase letters, numbers, special characters, and have a minimum length of 8 characters',
      );
      isValid = false;
    }

    if (password !== confirmPassword) {
      setConfirmPasswordError('Passwords do not match');
      isValid = false;
    }

    if (isValid) {
      // Load existing users from AsyncStorage or initialize an empty array
      const existingUsersString = await AsyncStorage.getItem('users');
      const existingUsers = existingUsersString ? JSON.parse(existingUsersString) : [];

      // Check if the username already exists
      const isUsernameExist = existingUsers.some(user => user.username === username);
      if (isUsernameExist) {
        alert('Username already exists. Please choose a different username.');
        return;
      }

      // Add the new user to the array
      const newUser = { username, password };
      existingUsers.push(newUser);

      // Save the updated array back to AsyncStorage
      await AsyncStorage.setItem('users', JSON.stringify(existingUsers));

      // Save the user to Redux state
      dispatch(setUser({ username }));

      // Navigate to the desired screen (e.g., Bottomtab)
      navigation.navigate('Bottomtab');
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.Container}>
      <View style={styles.Con2}>
        <Text style={styles.Text1}>Welcome </Text>
        <Text style={styles.Text2}>Sign up to continue</Text>
      </View>
      <View style={styles.Con3}>
        <View style={{ marginTop: '3%' }}>
          <Text style={styles.Text3}>Username</Text>
        </View>
        <View style={{ marginTop: '3%' }}>
          <TextInput
            style={styles.TextInput1}
            placeholder="Enter your mail-id here"
            value={username}
            onChangeText={text => setUsername(text)}
          />
        </View>
        <Text style={styles.Text4}>Password</Text>

        <TextInput
          style={styles.TextInput1}
          placeholder="Enter your Password"
          secureTextEntry
          value={confirmPassword}
          onChangeText={text => setConfirmPassword(text)}
        />
        <Text style={styles.errorText}>{passwordError}</Text>
        <Text style={styles.Text4}>Confirm Password</Text>
        <TextInput
          style={styles.TextInput1}
          placeholder="Confirm your Password"
          secureTextEntry
          value={password}
          onChangeText={text => setPassword(text)}
        />
        <Text style={styles.errorText}>{confirmPasswordError}</Text>
      </View>
      <View style={{ marginTop: '5%' }}>
        <Pressable style={styles.PressableText}>
          <Text style={styles.Text5}>Log In</Text>
        </Pressable>
      </View>

      <View style={styles.Con4}>
        <Pressable style={styles.PressableBtn} onPress={handleSignUp}>
          <Text style={styles.Btn}>Sign Up</Text>
        </Pressable>
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    flexDirection: 'column',
    marginHorizontal: 20,
  },
  Con1: {
    height: '10%',
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  Con2: {
    marginHorizontal: 20,
    marginTop: '12%',
  },
  Text1: {
    color: 'black',
    fontSize: 35,
  },
  Text2: {
    fontSize: 20,
    marginTop: 8,
  },
  Con3: {
    height: '40%',
    justifyContent: 'flex-start',
    width: '90%',
    marginHorizontal: 20,
    marginTop: '10%',
  },
  Text3: {
    fontSize: 25,
    color: 'black',
    justifyContent: 'flex-start',
  },
  TextInput1: {
    borderBottomWidth: 0.2,
    color: 'black',
    fontSize: 18,
  },
  Text4: {
    fontSize: 25,
    color: 'black',
    marginTop: 20,
  },
  PressableText: {
    flexDirection: 'row-reverse',
    height: '5%',
    justifyContent: 'flex-start',
    marginTop: -20,
  },
  Text5: {
    color: 'black',
    fontSize: 20,
    marginHorizontal: 20,
  },
  Con4: {
    marginTop: '7%',
    height: '30%',
    justifyContent: 'flex-start',
  },
  PressableBtn: {
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#f96060',
    height: 50,
    width: '90%',
    borderRadius: 8,
    marginTop: 30,
  },
  Btn: {
    color: 'white',
    fontSize: 20,
    alignSelf: 'center',
    justifyContent: 'center',
  },
});

export default SignUp;










