//authActions.js
import { useSelector } from "react-redux";

export const setUser = (user) => ({
    type: 'SET_USER',
    payload: user,
  });
  
  export const clearUser = () => ({
    type: 'CLEAR_USER',
  });
  
  export const addNote = (note) => ({
    type: 'ADD_NOTE',
    payload: note,
  });








