//noteReducer.js
const initialState = {
    notes: [],
    notesWithColor: [],
  };
  
  const noteReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'ADD_NOTE':
        return { ...state, notes: [...state.notes, action.payload],
          notesWithColor: [...state.notesWithColor, action.payload],
         };

  
      default:
        return state;
    }
  };
  
  export default noteReducer;






