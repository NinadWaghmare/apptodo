//rootReducer.js
import { combineReducers } from 'redux';
import authReducer from './reducers/authReducer'
import noteReducer from './reducers/noteReducer' 

const rootReducer = combineReducers({
  auth: authReducer,
  notes: noteReducer, 
});

export default rootReducer;



